import { Component, OnInit } from '@angular/core';
import { SpinnerService } from 'src/app/services/spinner.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'github-api';
  loaderPhrase = 'Cargando...';

  constructor(
    public spinnerService: SpinnerService
  ){}

  ngOnInit(){
    this.spinnerService.spinnerEvent.subscribe((val) => {
      this.loaderPhrase = val;
    });
  }

}


