import { Component, OnInit } from '@angular/core';
import { SpinnerService } from 'src/app/services/spinner.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public email: string;
  public password: string;

  constructor(
    public spinnerService: SpinnerService,
    public authService: AuthService
  ) { }

  ngOnInit(): void {
  }

  // Mandar llamar el servicio de iniciar sesión
  public async signIn(email: string, password: string): Promise<void>{
    this.spinnerService.showSpinner();

    this.authService.signIn(email, password);
  }

}
