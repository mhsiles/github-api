import { Component, OnInit } from '@angular/core';
import { GithubService } from 'src/app/services/github.service';
import { SpinnerService } from 'src/app/services/spinner.service';

@Component({
  selector: 'app-repository-info',
  templateUrl: './repository-info.component.html',
  styleUrls: ['./repository-info.component.css']
})
export class RepositoryInfoComponent implements OnInit {

  public repositoryInfo = {};
  public username: string;
  public repository: string;
  public error: string = '';
  private timer;

  constructor(
    private githubService: GithubService,
    public spinnerService: SpinnerService
  ) { }

  // Suscripción al servicio de github
  ngOnInit(): void {
    if (this.githubService.repositoriesList){
      this.repositoryInfo = this.githubService.repositoryInfo;
    }
    this.githubService.repositoryInfoEvent.subscribe( information => {
      this.repositoryInfo = information;
    });
  }

  // Hacer la búsqueda del repositorio específico
  public searchRepo(): any{
    this.spinnerService.showSpinner();
    this.githubService.getUserRepo(this.username, this.repository);
  }

}
