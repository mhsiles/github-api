import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { GithubService } from 'src/app/services/github.service';
import { SpinnerService } from 'src/app/services/spinner.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  public repositories = [];
  public username: string;
  public error: string = '';
  private timer;

  constructor(
    private githubService: GithubService,
    public spinnerService: SpinnerService
    
  ) { }

  // Suscripción al servicio de Github
  ngOnInit(): void {
    this.spinnerService.showSpinner();

    if (this.githubService.repositoriesList){
      this.repositories = this.githubService.repositoriesList;
    }
    this.githubService.repositoriesEvent.subscribe( repositoriesList => {
      this.repositories = repositoriesList;
    });

    this.spinnerService.hideSpinner();
  }

  // Búsqueda de usuarios Github
  public searchUserRepos(): any{
    this.spinnerService.showSpinner();

    this.githubService.getAllUserRepos(this.username);
  }

}

