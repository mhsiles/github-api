import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @Input()
  user: User;

  constructor(
    public authService: AuthService
  ) { }

  ngOnInit(): void {
  }

  signOut(){
    this.authService.signOut();
  }

}
