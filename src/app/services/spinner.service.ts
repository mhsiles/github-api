import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SpinnerService {

  public spinnerEvent = new Subject<string>();

  constructor(
    public spinner: NgxSpinnerService) {
  }

  public showSpinner() {
    this.spinnerEvent.next('Cargando...');
    this.spinner.show();
  }

  public hideSpinner() {
    this.spinner.hide();
  }

}
