import { Injectable, NgZone } from '@angular/core';
import { User } from '../models/user.model';
import { auth } from 'firebase/app';
import { BehaviorSubject } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SpinnerService } from './spinner.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  public userData: User;
  public userEvent: BehaviorSubject<any> = new BehaviorSubject<any>({});
  firebaseErrors = {
    'auth/user-not-found': 'Usuario no encontrado.',
    'auth/invalid-email': 'El formato del correo es inválido, intente nuevamente.',
    'auth/wrong-password': 'Contraseña inválida. Favor de verificarla e intente nuevamente.'
  };

  constructor(
    public afs: AngularFirestore,
    private afAuth: AngularFireAuth,
    public router: Router,
    public ngZone: NgZone,
    private toastr: ToastrService,
    public spinnerService: SpinnerService,
  ) {
    /* Guardar la info en local storage al inicar sesión */
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userData = user;
        this.userEvent.next(this.userData);
        localStorage.setItem('user', JSON.stringify(this.userData));
        JSON.parse(localStorage.getItem('user'));
      } else {
        localStorage.setItem('user', null);
        this.userEvent.next({});
        JSON.parse(localStorage.getItem('user'));
      }
    })
  }

  // Sign in with email/password
  async signIn(email, password): Promise<any> {
    return this.afAuth.signInWithEmailAndPassword(email, password)
      .then((result) => {
        this.ngZone.run(() => {
          this.spinnerService.hideSpinner();
          this.toastr.success('Bienvenido.', '¡Éxito!');
          this.SetUserData(result.user);
          window.location.href = '/github';
        });
      }).catch((error) => {
        this.spinnerService.hideSpinner();
        this.toastr.error(this.firebaseErrors[error.code] || error.message, 'Error');
      });
  }


  // Verificar que está loggeado
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return user !== null && user.emailVerified !== false ? true : false;
  }

  // Sign out
  public signOut(): any {
    
    return this.afAuth.signOut().then(() => {
      localStorage.removeItem('user');
      this.router.navigate(['/']);
    });
  }

  /* Guarda la info del usuario */
  public SetUserData(user): any {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const userData: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified
    }
    return userRef.set(userData, {
      merge: true
    })
  }

  /*
  *
  */
}
