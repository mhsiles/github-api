import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { SpinnerService } from './spinner.service';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class GithubService {

  public api_url = 'https://api.github.com'
  public repositoriesList: any[] = [];
  public repositoriesEvent: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  public repositoryInfo = {};
  public repositoryInfoEvent: BehaviorSubject<any> = new BehaviorSubject<any>({});

  constructor(
    private http: HttpClient,
    public spinnerService: SpinnerService,
    private toastr: ToastrService,
  ) { }

  public async getAllUserRepos(user: string): Promise<any>{
    this.http.get<any[]>(this.api_url + "/users/" + user + "/repos").subscribe(
      res => {
        this.repositoriesList = res;
        this.repositoriesEvent.next(this.repositoriesList);
        this.showSuccess();
      },
      err => {
        this.repositoryInfo = null;
        this.repositoryInfoEvent.next(this.repositoryInfo);
        this.showError(err.error.message);
      },
      () => {
        this.spinnerService.hideSpinner();
      }
      )
    }
    
    public async getUserRepo(user: string, repo: string): Promise<any>{
      this.http.get<any[]>(this.api_url + "/repos/" + user + "/" + repo).subscribe(
        res => {
          this.repositoryInfo = res;
          this.repositoryInfoEvent.next(this.repositoryInfo);
          this.showSuccess();
      },
      err => {
        this.repositoryInfo = {};
        this.repositoryInfoEvent.next(this.repositoryInfo);
        this.showError(err.error.message);
      },
      () => {
        this.spinnerService.hideSpinner();
      }
    )
  }

  private showSuccess(): void{
    this.toastr.success('Consulta realizada exitosamente', '¡Éxito!');
  }

  private showError(message: string): void{
    this.spinnerService.hideSpinner();
    this.toastr.error(message, 'ERROR');
  }

}
