import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from 'src/app/shared/guard/auth.guard';
// main component
import { SessionLayoutComponent } from './session-layout/session-layout.component';
// children components
import { LoginComponent } from 'src/app/components/session/login/login.component';

export const catalogueModuleRoutes: Routes = [
    {
        path: '',
        component: SessionLayoutComponent,
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'login'
            },
            {
                path: 'login',
                component: LoginComponent
            }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(catalogueModuleRoutes)],
  exports: [RouterModule]
})
export class SessionRoutingModule { }
