import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SessionRoutingModule } from './session-routing.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule } from '@angular/forms';

import { SessionLayoutComponent } from './session-layout/session-layout.component';
// import {DataTablesModule} from 'angular-datatables';

@NgModule({
  declarations: [
    SessionLayoutComponent,
  ],
  imports: [
    CommonModule,
    SessionRoutingModule,
    FontAwesomeModule,
    FormsModule,
  ],
})
export class SessionModule {}
