import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-github-layout',
  templateUrl: './github-layout.component.html',
  styleUrls: ['./github-layout.component.scss']
})
export class GithubLayoutComponent implements OnInit {

  public user: User;

  constructor(
    public router: Router,
    public authService: AuthService
  ) { }

  ngOnInit(): void {
    if (this.authService.userData){
      this.user = this.authService.userData;
    }
    this.authService.userEvent.subscribe(user => {
      this.user = user;
    })
  }

}
