import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from 'src/app/shared/guard/auth.guard';
// main component
import { GithubLayoutComponent } from './github-layout/github-layout.component';
// children components
import { UserListComponent } from 'src/app/components/user/user-list/user-list.component';
import { RepositoryInfoComponent } from 'src/app/components/repository/repository-info/repository-info.component';

export const catalogueModuleRoutes: Routes = [
    {
        path: '',
        canActivate: [ AuthGuard ],
        component: GithubLayoutComponent,
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'usuarios'
            },
            {
                path: 'usuarios',
                component: UserListComponent
            },
            {
                path: 'repositorios',
                component: RepositoryInfoComponent
            }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(catalogueModuleRoutes)],
  exports: [RouterModule]
})
export class GithubRoutingModule { }
