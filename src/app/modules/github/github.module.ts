import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GithubRoutingModule } from './github-routing.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule } from '@angular/forms';

import { SidebarComponent } from 'src/app/components/shared/sidebar/sidebar.component';
import { GithubLayoutComponent } from './github-layout/github-layout.component';
import { RepositoryInfoComponent } from 'src/app/components/repository/repository-info/repository-info.component';
import { UserListComponent } from 'src/app/components/user/user-list/user-list.component';
// import {DataTablesModule} from 'angular-datatables';

@NgModule({
  declarations: [
    GithubLayoutComponent,
    RepositoryInfoComponent,
    UserListComponent,
    SidebarComponent,
  ],
  imports: [
    CommonModule,
    GithubRoutingModule,
    FontAwesomeModule,
    FormsModule,
  ],
})
export class GithubModule {}
