import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './shared/guard/auth.guard';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'inicio'
  },
  {
    path: 'inicio',
    canActivate: [AuthGuard],
    loadChildren: () => import('./modules/session/session.module').then(mod => mod.SessionModule)
  },
  {
    path: 'github',
    canActivate: [AuthGuard],
    loadChildren: () => import('./modules/github/github.module').then(mod => mod.GithubModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
